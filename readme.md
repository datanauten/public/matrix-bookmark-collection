# generic

[matrix.org](https://matrix.org)


# kubernetes

[Element On-Premise Documentation](https://ems-docs.element.io/books/element-on-premise-documentation) | [Helm Charts](https://gitlab.com/ananace/charts) | 

# widgets 
[matrix-widget-api](https://github.com/matrix-org/matrix-widget-api)


# documentation 
https://doc.matrix.tu-dresden.de/